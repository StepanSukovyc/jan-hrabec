<%@ include file="/init.jsp"%>

<script type="text/javascript">
	function callAPI() {
		var xhr = new XMLHttpRequest();
		xhr
				.open(
						'GET',
						'http://localhost:7777/loadinternalfile?filename=catList',
						true);
		xhr.onload = function() {
			if (xhr.readyState === 4 && xhr.status === 200) {
				document.getElementById('result').textContent = xhr.responseText;
			} else {
				document.getElementById('result').textContent = 'Error calling API: '
						+ xhr.statusText;
			}
		};
		xhr.onerror = function() {
			document.getElementById('result').textContent = 'Error calling API: '
					+ xhr.statusText;
		};
		xhr.send(null);
	}
</script>

<div class="loadfile-container">
    <div class="loadfile-content">
        <h1>Obsah souboru</h1>
        <p id="result" class="filecontent-text"/>
    </div>

    <div class="loadfile-container-footer">
        <button id="loadFileButton" onclick="callAPI()">Nacist</button>
    </div>
</div>