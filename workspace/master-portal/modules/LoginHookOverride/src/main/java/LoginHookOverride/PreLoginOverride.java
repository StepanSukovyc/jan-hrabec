package LoginHookOverride;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;

import org.osgi.service.component.annotations.Component;

/**
 * @author Hondes
 */
@Component(immediate = true, property = { "key=login.events.pre" }, service = LifecycleAction.class)
public class PreLoginOverride implements LifecycleAction {

	@Override
	public void processLifecycleEvent(LifecycleEvent lifecycleEvent) throws ActionException {
		System.out.println("Override login pre");
	}
}