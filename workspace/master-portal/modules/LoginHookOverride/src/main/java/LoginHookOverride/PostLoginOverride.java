package LoginHookOverride;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Hondes
 */
public class PostLoginOverride extends Action {

	@Override
	public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {
		try {
			System.out.println("Post login override, redirect /filecontent");
			
			response.sendRedirect("/filecontent");
		} catch (Exception e) {
			throw new ActionException(e);
		}
	}

}
