const port = 7777;

import { router } from "./routes/fileRoute.js"
import express from "express"
import OpenApiValidator from "express-openapi-validator"
import cors from "cors"
const app = express()

app.use(cors({
    origin: '*'
}));
app.use(express.json())
app.use(router)

app.use(
    OpenApiValidator.middleware({
        apiSpec: './openapi.yaml',
        validateRequests: true, // (default)
        validateResponses: true, // false by default
    }),
);

app.use((err, req, res, next) => {
    // format error
    res.status(err.status || 500).json({
        message: err.message,
        errors: err.errors,
    });
});

app.listen(port, () => {
    console.log("Server is listening on port: " + port)
});