import { Router } from "express";
import fs from "fs/promises"

const rootRouter = Router();

rootRouter.get('/loadinternalfile', async (req, res) => {
    const filePath = "./files/" + req.query.filename + ".txt";

    try {
        const data = await fs.readFile(filePath, "utf8");
	  console.log("File served");
        res.status(200).send(data);
    } catch (err) {
        res.status(404).send("Soubor nenalezen");
    }
});

export { rootRouter as router }